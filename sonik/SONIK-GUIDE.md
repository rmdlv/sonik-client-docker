# Инструкция по установке sonik-client

## Вступление
For instruction in English, please, see: [SONIK-GUIDE-ENG.md](SONIK-GUIDE-ENG.md) 

Для установки клиента сети СОНИК используйте данную инструкцию.<br>
Рекомендованный к использованию дистрибутив — Debian 11 (bullseye), и это руководство будет адаптировано для него, но отличия от других должны быть лишь небольшими.<br>

## Базовое описание Докера
[Официальный обзор](https://docs.docker.com/get-started/overview/)

***Образы (images)*** обычно размещаются в реестре, например hub.docker.com, откуда вы можете загрузить их в свою систему.
Это полный пакет программного обеспечения для запуска приложения, в данном случае образ Debian и множество установленных пакетов, необходимых для sonik-client.<br>
***Контейнер (container)***  это работающий экземпляр изображения и, по сути, представляет собой изолированную среду, в которой вы запускаете приложение.
Они всегда начинаются с образа и могут быть изменены, но они непостоянны, поэтому после остановки все изменения теряются.<br>
***Тома (volumes)*** — это либо постоянное хранилище для контейнеров, либо привязка к вашему хосту для настройки или хранения.

***Создание (compose)*** В этом файле указаны все различные сервисы (по сути, контейнеры), а также все их настройки и отношения между ними.
В этой конфигурации контейнеры существуют в отдельной сети, поэтому rigctld работает в своем собственном контейнере, и клиент общается с ним через эту сеть.
Это означает, что вам не нужна куча скриптов для запуска/остановки/обновления всего.
Это также означает, что вы не запускаете несколько сервисов в одном контейнере.
По умолчанию он создает стэк, названный в честь каталога, в котором находится файл компоновки.

***Стэк (stack)*** В данном контексте, это результирующие контейнеры, сеть, тома и т. д., которые создаются и управляются с помощью Compose.

# Запуск клиента

## Подготовка системы хоста

### Raspberry Pi
Для начала, если ваша система полностью свежая, вам необходимо:
1. Установить настройки локали (**Locale settings**)
2. Расширить память системы (**Expand filesystem**)

После этого необходимо перезагрузить систему.<br>
```shell
sudo reboot
```

### OrangePi
TBD

## Установка Docker Engine (docker.com)

Далее необходимо установить Docker Engine. Как это сделать для вашей системы можно узнать здесь: [docker installation](https://docs.docker.com/engine/install/debian/)

Если же вы используете Raspberry Pi OS 64bit or 32bit Lite (bullseye), то необходимо выполнить следующие действия:
```shell
# уже установлено: ca-certificates curl lsb-release
# опционально: tmux uidmap
sudo apt update
sudo apt upgrade -y
sudo apt install -y ca-certificates curl gnupg lsb-release git

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker.gpg
echo "deb https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# добавление пользователя в группу докера, чтобы не использовать sudo, для применения перезайти
sudo adduser $(whoami) docker
```
Чтобы изменения по правам группы вступили в силу, необходимо перезайти.
```shell
# введите
exit
# и подключитесь заново
```

## Установка Docker.io

Причина, по которой используются backports в том, что версия compose в bullseye - 1.25 и отсутствует поддержка cgroup, используя backport получим версию 1.27.<br>
Если ваш дистрибутив не имеет backports, добавьте, используя следующие команды, и после этого повторите установку docker-compose еще раз:
```shell
echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free" | sudo tee /etc/apt/sources.list.d/backports.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  648ACFD622F3D138 0E98404D386FA1D9
sudo apt update
```

Если вы используете Debian 12 bookworm, установка должна быть проще, так как система должна иметь необходимые пакеты.
```shell
sudo apt install docker.io apparmor docker-compose
```

В случае Debian 11 bullseye будем использовать backports для установки более новой версии:
```shell
sudo apt install docker.io apparmor
sudo apt -t bullseye-backports install docker-compose
```

Если у вас не удается установить необходимую версию compose на ваш дистрибутив, пожалуйста, следуйте следующей [официальной инструкции](https://docs.docker.com/compose/install/linux/#install-the-plugin-manually).<br>

Также можно воспользоваться скриптом от Даниеля SA2KNG, который берет последнюю версию compose и устанавливает, используя buildx: [update-docker-cli.sh](../update-docker-cli.sh):
```shell
wget https://github.com/kng/satnogs-client-docker/raw/main/addons/update-docker-cli.sh
chmod 0755 update-docker-cli.sh
./update-docker-cli.sh
```

## Конфигурация

### udev права и module blacklist

На этом шаге нужно будет установить библиотеки и драйверы для вашего SDR устройства, включая udev права и module blacklist.<br>
Дополнительное ПО, как, например, soapysdr, не является необходимым на хосте, но безусловно может быть установлено, если вы хотите, например, поработать с SDR устройством в ручном режиме (не используя СОНИК) или протестировать его.<br>

Постарайтесь, сохранять часы хоста синхронизированными, это очень важно и легко решается, используя ntp.
Если вы используете RTL-SDR, то выполните следующие команды:
```shell
sudo apt install rtl-sdr ntp
echo "blacklist dvb_usb_rtl28xxu" | sudo tee /etc/modprobe.d/blacklist-rtlsdr.conf
sudo modprobe -r dvb_usb_rtl28xxu
```

Как альтернатива, а также при использовании других SDR устройств, чтобы установить необходимый драйвер на хост, можно воспользоваться двумя файлами, которые необходимо установить.
Есть два основных концепта sdr драйверов - udev права и module blacklist. Здесь мы собрали все нам необходимое.

Просто скопируйте [10-sonik.rules](10-sonik.rules) в `/etc/udev/rules.d/` и [sonik-blacklist.conf](sonik-blacklist.conf) в `/etc/modprobe.d/`. 
Также не забудьте установить ntp.
```
sudo apt install ntp
wget https://gitlab.com/space-education-development/sonik/sonik-client-docker/-/raw/sonik/sonik/sonik-blacklist.conf
wget https://gitlab.com/space-education-development/sonik/sonik-client-docker/-/raw/sonik/sonik/10-sonik.rules

sudo cp 10-sonik.rules /etc/udev/rules.d/
sudo cp sonik-blacklist.conf /etc/modprobe.d/
```
Самый простой способ применить изменения - перезагрузка. Можно и в ручную, с помощью `rmmod` и `udevadm control --reload-rules && udevadm trigger`.

### Конфигурация вашей станции

Начните с создания директории с именем (номером) вашей станции, оно будет показываться далее в нескольких местах после сборки.
Это также позволит разделять несколько станций, работающих на одном хосте.
Например, для станции номер 101:
```shell
mkdir -p station-101
cd station-101
```

Далее скачайте конфигурационные файлы:
```shell
wget https://gitlab.com/space-education-development/sonik/sonik-client-docker/-/raw/sonik/sonik/docker-compose.yml
wget https://gitlab.com/space-education-development/sonik/sonik-client-docker/-/raw/sonik/sonik/station.env
```
Файл `station.env` содержит все переменные станции, некоторые переменные, которые важны для работы сборки находятся в файле `docker-compose.yml`.
Используйте ваш любимый текстовый редактор для конфигурации:
```shell
nano station.env
```
Будьте внимательны, чтобы все изначально добавленные активные строки остались незакомментированными, так как они важны для работы станции.
Также учтите, что значения не должны быть в кавычках.

Все переменные в sonik-client соответствуют последней версии satnogs-client. Узнать больше вы можете [здесь](https://docs.satnogs.org/projects/satnogs-client/en/stable/userguide.html#environment-variables).

## Поднимаем стэк

В `docker-compose` (на некоторых системах - `docker compose`) программа, контролирующая создание, обновление, сборку и выполнение стэка.
Простые команды, которые вы будете использовать - `docker-compose up -d` и `docker-compose down`.
Когда вы изменяете файл `docker-compose.yml` или конфигурацию станции, она будет решать, что необходимо выполнить для поднятия стэка в соответствии с тем, что было изменено.
Например, при изменении усиления SDR, она пересоздаст и перезапустит клиент, а не rigctld.

Запуск стэка (и его обновление после изменения конфигурации):
```shell
docker-compose up -d
```

Остановка стэка:
```shell
docker-compose down
```

Обновление образов и запуск стэка:
```shell
docker-compose pull
docker-compose up -d
```

Спустя время будут накапливаться старые образы, они могут быть удалены с помощью `docker image prune -af`

## Мониторинг и поддержка

Внутри каждого контейнера есть логи, которые выводятся в stdout, что делает их видимыми изнутри контейнера.
Запуск для отслеживания запущенного стэка:
```shell
docker-compose logs -f
```

Если вы хотите запустить команды внутри контейнера, это можно сделать следующей командой:
```shell
docker-compose exec satnogs_client bash
```
Контейнер может быть любым из запущенных сервисов, но его нельзя остановить. Чтобы выйти используйте Ctrl-D или наберите `exit`.

# Дополнительные сервисы, эксперименты и аддоны
Также, используя файл `docker-compose.yml`, может быть подключено поворотное устройство.
```shell
# TBD
```

## Экспериментальные сборки
В процессе разработки может добавляться или обновляться функционал клиента, но перед этим он должен быть протестирован. Возникают экспериментальные сборки. Их работа может быть не до конца проверена и быть в процессе активной разработки. Такие образы помечены словом unstable (нестабильные).

Изменяя [docker-compose.yml](docker-compose.yml) и спускаясь к сервису satnogs_client, ключ`image:` обозначает используемый образ.
При желании изменить версию клиента просто закомментируйте текущую и раскомментируйте ту, которую хотите использовать. Например:
```yaml
  satnogs_client:
    #image: registry.gitlab.com/librespacefoundation/satnogs/satnogs-client/satnogs-client:master  # LSF stable docker image
    image: registry.gitlab.com/librespacefoundation/satnogs/satnogs-client/satnogs-client:master-unstable  # LSF experimental docker image
```
На данный момент в [docker-compose.yml](docker-compose.yml) доступны несколько источников образов:
* Сборки проекта СОНИК (sonik-client). Список версий (ссылка TBD).
* Сборки LibreSpaceFoundation. Список доступных версий есть на [gitlab registry](https://gitlab.com/librespacefoundation/satnogs/satnogs-client/container_registry/3553292) и на [dockerhub](https://hub.docker.com/r/librespace/satnogs-client/tags), доступно два тэга (TAG): master и master-unstable.
* Сборки Даниеля SA2KNG. Источник (ссылка TBD).

<br>После изменения пересоберите контейнер`docker-compose up -d`.

## Аддоны (Addons)
Образ с аддонами задокументирован [здесь](../addons/README.md), он содержит намного больше функционала, включая интеграцию с gr-satellites (ссылка TBD), что позволяет демодулировать сигнал с большего количества спутников.<br>
Интеграция с gr-satellites (ссылка TBD) и аддоны могут быть активированы выбором соответствующего образа (`image:`) в сервисе satnogs_client. Они помечены добавлением *-addons*.<br>

Для работы версии с аддонами необходимо наличие следующих раскомментированных строк в файле `station.env`:
```
SATNOGS_PRE_OBSERVATION_SCRIPT=satnogs-pre {{ID}} {{FREQ}} {{TLE}} {{TIMESTAMP}} {{BAUD}} {{SCRIPT_NAME}}
SATNOGS_POST_OBSERVATION_SCRIPT=satnogs-post {{ID}} {{FREQ}} {{TLE}} {{TIMESTAMP}} {{BAUD}} {{SCRIPT_NAME}}
UDP_DUMP_HOST=0.0.0.0
```

Аддоны включают в себя очень много функционала. В [репозитории (TBD)](https://github.com/kng/satnogs-client-docker/tree/main/addons) можно узнать больше.

## Разработка и сборка образов под свою архитектуру
Для некоторых платформ может не быть готовых сборок, а текущие могут не поддерживаться. Например, можно увидеть такой вывод в `docker-compose logs -f`:
TBD

Для решения проблемы необходимо собрать образ клиента на своей системе, где будет работать станция.

Для этого необходимо запустить скрипт `build.sh`, который выполнит сборку клиента на вашем хосте. Если вы хотите изменить тег (TAG) образа локальной сборки, это можно сделать изменив TAG в этой строчке (ссылка TBD) файла `build.sh`. По-умолчанию, установлен TAG с ветки репозитория, на которой вы находитесь. После также можно собрать и версию с аддонами, запустив `build-addons.sh`. Все вместе: 
```shell
git clone --recurse-submodules https://gitlab.com/space-education-development/sonik/sonik-client-docker.git
cd sonik-client-docker/client
./build.sh
./build-addons.sh
```

Если вы не меняли тег образа, то вам необходимо просто пересобрать контейнер`docker-compose up -d`. При изменении тега, в файле `docker-compose.yml` в сервисе satnogs_client необходимо указать правильный тег образа (`image:`).


## Запуск нескольких станций
TBD, разные ID папки, управление rtl-sdr через ID.

## Рекомендуется установить: [Portainer](https://docs.portainer.io/start/install/server/docker/linux)
Он предоставляет web-интерфейс для слежением за контейнерами. Выполните:

```shell
docker volume create portainer_data
docker run -d \
    -p 8000:8000 \
    -p 9443:9443 \
    --name portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce:latest
```

Перейдите по ссылке в браузере https://127.0.0.1:9443 (исправив на корректный хост в сети) и следуйте инструкции, используя локальный сокет, в секции "Get started".
