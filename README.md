# СОНИК клиент (docker)

Данный репозиторий содержит в себе все необходимое для подготовки работы клиента сети [СОНИК](https://sonik.space/). Для установки используются образы с [sonikspace](https://hub.docker.com/u/sonikspace), а также вы можете собрать собственный образ клиента под свою систему.<br>

Для начала работы рекомендуется ознакомиться с [инструкцией](sonik/SONIK-GUIDE.md) (English version of [GUIDE](sonik/SONIK-GUIDE-ENG.md)).<br>
Коллекция [аддонов](addons/README.md) от SA2KNG для клиента сети СОНИК.<br>
Для сборки клиента обратитесь к [инструкции](client/README.md).

Основные репозитории:
* [docker-hamlib (LSF)](https://gitlab.com/librespacefoundation/docker-hamlib)
* [docker-gnuradio (LSF)](https://gitlab.com/librespacefoundation/docker-gnuradio)
* [sonik-client](https://gitlab.com/space-education-development/sonik/sonik-client)
