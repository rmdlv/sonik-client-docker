# Сборка клиента для сети СОНИК

## Введение
Данная сборка основана на работе Даниэля SA2KNG [satnogs-client-docker](https://github.com/kng/satnogs-client-docker/tree/main/sa2kng).
В настоящий момент, репозитории, на которых основан клиент находятся в [build.sh](build.sh) и состоят из gr-satnogs, gr-soapy, satnogs-flowgraphs и sonik-client.

## Установка
Чтобы собрать образ, используйте: `./build.sh`<br>
После к сборке вы можете добавить аддоны. Для этого выполните `./build-addons.sh`<br>

Выполнив это, соберутся образы с тэгами, указанными в [build.sh](build.sh) и [build-addons.sh](build-addons.sh): `sonikspace/sonik-client:1.2.2` и `sonikspace/sonik-client:1.2.2-addons`. Тэги (1.2.2 и 1.2.2-addons) вы можете поменять вручную в соответствующих файлах для своей сборки.

Чтобы их использовать, обратитесь к главной инструкции [SONIK-GUIDE](../sonik/SONIK-GUIDE.md). При изменении тэга, не забудьте исправить его также в сервисе `sonik_client:` в файле `docker-compose.yml`.
