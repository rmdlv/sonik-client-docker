#!/bin/bash
export DOCKER_BUILDKIT=1
TAG="1.2.2-addons"
REPO_ROOT="sonikspace"
SATNOGS_IMAGE_TAG="1.2.2"
ARGS="
    --build-arg SATNOGS_IMAGE_TAG=${SATNOGS_IMAGE_TAG} \
    --build-arg REPO_ROOT=${REPO_ROOT} \
"

docker build \
    -t ${REPO_ROOT}/satnogs-client:${TAG} \
    ${ARGS} \
    ../addons "$@"
